# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build


# add FTS repo
ADD http://fts-repo.web.cern.ch/fts-repo/fts3-continuous-el7.repo /etc/yum.repos.d/fts.repo

#add DMC repo
ADD http://dmc-repo.web.cern.ch/dmc-repo/dmc-ci-el7.repo /etc/yum.repos.d/dmc.repo

# add trustanchors
ADD http://repository.egi.eu/sw/production/cas/1/current/repo-files/EGI-trustanchors.repo /etc/yum.repos.d/EGI-trustanchors.repo

#add install certificates
RUN yum install -y ca-policy-egi-core fetch-crl

#install the voms stuff
RUN yum install -y voms voms-clients voms-config-wlcg voms-config-vo-dteam

RUN yum install -y  nordugrid-arc-client

RUN yum install -y golang

RUN yum install -y strace openssl-devel

